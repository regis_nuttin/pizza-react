import React from "react"
import {BrowserRouter as Router} from "react-router-dom"
import {GlobalStyle} from "./globalStyles"
import Hero from "./components/Hero/index"
import Products from "./components/Products/index"
import {productData} from "./components/Products/data"
import Feature from "./components/feature"
import Footer from "./components/Footer/index"

function App() {
  return (
    <Router>
      <GlobalStyle/>
      <Hero/>
      <Products heading="Choisissez votre pizza" data={productData}/>
      <Feature/>
       <Products heading="Nos meilleurs pizzas" data={productData}/>
       <Footer/>
    </Router>
  );
}

export default App;
