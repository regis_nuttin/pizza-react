import React from 'react'
import {SidebarContainer, Icon, CloseIcon, SidebarMenu, SidebarLink, SidebarRoute, SidebtnWrap} from "./SidebarElements"


const Sidebar = ({isOpen, toggle}) => {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <Icon>
            <CloseIcon/>
            </Icon>
<SidebarMenu>
<SidebarLink to="/">Pizzas</SidebarLink>
<SidebarLink to="/">Desserts</SidebarLink>
<SidebarLink to="/">Menu</SidebarLink>
</SidebarMenu>

<SidebtnWrap>
    <SidebarRoute to="/">Commandez</SidebarRoute>
</SidebtnWrap>

        </SidebarContainer>
    )
}

export default Sidebar
