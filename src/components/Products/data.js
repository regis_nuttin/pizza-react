import product1 from "../../images/pizza1.jpg"
import product2 from "../../images/pizza2.jpg"
import product3 from "../../images/pizza3.jpg"

export  const productData = [
    {
img : product1,
alt : "Pizza",
name:"Pizza Supreme",
desc:"Super pizza, tomates, basilique, sauce italienne, olives, et pesto",
price:"€19.99",
button: "Commandez"
    },
   {
img : product2,
alt : "Pizza",
name:"Pizza Cheese",
desc:"Super pizza, tomates, basilique, sauce italienne, olives, et pesto",
price:"€18.99",
button: "Commandez"
    },
       {
img : product3,
alt : "Pizza",
name:"Pizza Veggie",
desc:"Super pizza, tomates, basilique, sauce italienne, olives, et pesto",
price:"€16.99",
button: "Commandez"
    },
]