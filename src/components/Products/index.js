import React from 'react'
import {ProductsContainer,ProductsHeading, ProductsWrapper,ProductCard,ProductImg,ProductInfo,ProductTitle,ProductDesc,ProductPrice, ProductButton } from "./ProductsElements"
import AOS from 'aos';
import 'aos/dist/aos.css';

AOS.init();

const Products = ({heading, data}) => {
    return (
        <ProductsContainer >
            <ProductsHeading>
                {heading}
            </ProductsHeading>
            <ProductsWrapper>
                {data?.map((product,index)=>{return(
                    <ProductCard key={index}
                        data-aos="fade-up"
    data-aos-offset="50"
    data-aos-delay="50"
    data-aos-duration="800"
    data-aos-easing="ease-in-out"
    data-aos-mirror="true"
    data-aos-once="false"
  data-aos-anchor-placement="top-bottom">
<ProductImg src={product.img} alt={product.alt}/>
<ProductInfo>
    <ProductTitle>{product.name}</ProductTitle>
    <ProductDesc>{product.desc}</ProductDesc>
    <ProductPrice>{product.price}</ProductPrice>
    <ProductButton>{product.button}</ProductButton>
</ProductInfo>

                    </ProductCard>
                )})}
            </ProductsWrapper>
        </ProductsContainer>
    )
}

export default Products
