import React from 'react'
import {FeatureContainer,FeatureButton } from "../feature/FeatureElements"
import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
// ..
AOS.init();

const Feature = () => {
    return (
        <FeatureContainer>
            <h1>Pizza du jour</h1>
            <p>Super pizza, fromages, ananas</p>
            <FeatureButton
            
                      data-aos="fade-up"
    data-aos-offset="1"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-easing="ease-in-out"
    data-aos-mirror="true"
    data-aos-once="false"
    data-aos-anchor-placement="top-bottom">
                Appelez-nous
            </FeatureButton>
        </FeatureContainer>
    )
}

export default Feature
