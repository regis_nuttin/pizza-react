import React, {useState} from 'react'
import {HeroContainer, HeroContent, HeroItems, HeroH1, HeroP, HeroBtn} from "./HeroElement";
import Navbar from "../Navbar/index"
import Sidebar from '../Sidebar';
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();

const Hero = () => {

    const [isOpen, setIsOpen] = useState(false)

const toggle =()=>{
    setIsOpen(prev=>!prev)
}
    return (
        <HeroContainer>
            <Navbar toggle={toggle}/>
            <Sidebar toggle={toggle} isOpen={isOpen}/>
            <HeroContent    
    data-aos="fade-down"
    data-aos-offset="200"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-easing="ease-in-out">
                <HeroItems>
                    <HeroH1>Meilleurs pizzas</HeroH1>
                    <HeroP>Disponible en quelques minutes !</HeroP>
                    <HeroBtn>Commandez</HeroBtn>
                </HeroItems>
            </HeroContent>
        </HeroContainer>
    )
}

export default Hero
